package net.ihe.gazelle.pr.bean;

import org.junit.Test;
public class ISToolsTest {

    @Test(expected = Exception.class)
    public void getThumbnailOfPDFNull() throws Throwable {
        ISTools.getThumbnailOfPDF(null);
    }
}
