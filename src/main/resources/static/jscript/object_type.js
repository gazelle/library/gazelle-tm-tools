/**
 * @author		Abderrazek Boufahja - Abdallah Miladi / INRIA Rennes IHE development Project
 */

var resultColumnNumberForObject = 5;

function addToBodyOnload(newOnLoadFunction) {
	var existingOnload = window.onload;

	//Account for the fact that in some browsers (IE6) event is passed in, in others (FireFox2.0) its an implicit object.
	//Simply declaring the below as function(event) will not work in both!
	window.onload = function (oEvent) {

		if (!oEvent) {
			oEvent = event;
		}
		newOnLoadFunction(oEvent);
		return existingOnload(oEvent);
	}
}

function essai(){
	colorRowsOfTableForObject("form:ObjectTypeTable");
}

function newOnloadFunction(event) {
	colorRowsOfTableForObject("form:ObjectTypeTable");
}

function colorRowsOfTableForObject(idTable) {
	window.style.backgroundColor="#0000EE" ;
	var table = document.getElementById(idTable);
	table.style.backgroundColor = "#0000EE";
	/*
	var numberOfRows = table.rows.length;
	var stringStatus = "" ;

	for (var i = 2 ; i < numberOfRows; i++)  
	{
		colorRowForObject( table.rows[i]   )		
	}
	//*/
} 

function colorRowsOfCurrentTableForObject(table) {
	var numberOfRows = table.rows.length;
	var stringStatus = "" ;

	for (var i = 2 ; i < numberOfRows; i++)  
	{
		colorRowForObject( table.rows[i]   )		
	}
}

function colorRowForObject( idRow  )
{
	window.style.backgroundColor="#EE0000" ;
	var currentRow = idRow  ;

	var tdToUse = currentRow.cells[4] ;

	currentRow.style.backgroundColor="#0000EE" ;
	
	/*
	if (tdToUse != null) {
		var selectToUse = tdToUse.firstChild; 
		if ( selectToUse != null )
		{
			if (  selectToUse.value == "ready" ) // ready
				currentRow.style.backgroundColor="#CCFFCC" ;
			else if (  selectToUse.value == "deprecated" )     // deprecated
				currentRow.style.backgroundColor="#FF1200" ;
			else if (  selectToUse.value == "to be completed" )     // to be completed
				currentRow.style.backgroundColor="#FFCCCC" ;
			else if (  selectToUse.value == "storage/subtitute" )    // storage/subtitute
				currentRow.style.backgroundColor="#FFCC33" ;
			else if (  selectToUse.value == "Kudu import" )    // storage/subtitute
				currentRow.style.backgroundColor="#FFAACC" ;
			else{
				currentRow.style.backgroundColor="#E0E0E0" ;
			}
		}
	}
		*/
}