function changecss(myclass, element, value) {
	for ( var i = 0; i < document.styleSheets.length; i++) {
		var rules;
		if (document.styleSheets[i].cssRules) {
			rules = document.styleSheets[i].cssRules;
		} else {
			rules = document.styleSheets[i].rules;
		}

		for ( var j = 0; j < rules.length; j++) {
			if (rules[j] && rules[j].selectorText) {
				var selectorText = rules[j].selectorText;
				if (selectorText == myclass) {
					rules[j].style[element] = value;
				}
			}
		}
	}
}

function addToBodyOnresize(newOnResizeFunction) {
	var existingOnresize = window.onresize;

	window.onresize = function(oEvent) {

		if (!oEvent) {
			oEvent = event;
		}
		if (existingOnresize) {
			newOnResizeFunction(oEvent);
			return existingOnresize(oEvent);
		} else {
			return newOnResizeFunction(oEvent);
		}

	};
}

function addToBodyOnload(newOnLoadFunction) {
	var existingOnload = window.onload;

	window.onload = function(oEvent) {

		if (!oEvent) {
			oEvent = event;
		}

		if (existingOnload) {
			newOnLoadFunction(oEvent);
			return existingOnload(oEvent);
		} else {
			return newOnLoadFunction(oEvent);
		}
	};
}

function resizeColumns(event) {
	var totalWidth = $('globalform:systemsDiv').clientWidth - 20;
	var columns = totalWidth / 445;
	columns = Math.floor(columns);
	var rwidth = (totalWidth / columns) - 22 - 1;
	rwidth = Math.floor(rwidth);
	var width = rwidth + "px";
	var rwidthInfos = rwidth - 140;
	var widthInfos = rwidthInfos + "px";
	changecss(".pr-box", "width", width);
	changecss(".pr-infos", "width", widthInfos);
}

addToBodyOnresize(resizeColumns);
addToBodyOnload(resizeColumns);
addAjaxStopFunction(resizeColumns);
