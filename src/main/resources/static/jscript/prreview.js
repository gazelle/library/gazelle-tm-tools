function searchPdf(element) {
	var text = element.innerText;
	var pdfFrame = document.getElementById("pdfjs");
	// reference to window in the iframe
	var pdfWindow = pdfFrame.contentWindow;

	// reference to document in iframe
	var pdfDocument = pdfFrame.contentDocument ? pdfFrame.contentDocument
			: pdfFrame.contentWindow.document;

	pdfWindow.PDFFindBar.open();
	pdfDocument.getElementById('findHighlightAll').checked = true;
	pdfDocument.getElementById('findMatchCase').checked = false;
	pdfDocument.getElementById('findInput').value = text;
	pdfWindow.PDFFindBar.dispatchEvent('highlightallchange');
	pdfWindow.PDFFindBar.dispatchEvent('casesensitivitychange');
	pdfWindow.PDFFindBar.dispatchEvent('again', false);
}
