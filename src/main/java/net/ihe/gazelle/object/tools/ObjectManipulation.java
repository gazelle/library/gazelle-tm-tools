/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.object.tools;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.XMLRepresentationOfDicomObjectFactory;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.tree.XmlTreeDataBuilder;
import net.ihe.gazelle.objects.model.*;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.util.ThumbnailCreator;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.richfaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */
public class ObjectManipulation {

    private static Logger log = LoggerFactory.getLogger(ObjectManipulation.class);

    public ObjectManipulation() {
    }

    public List<ObjectInstanceFile> getListObjectInstanceFileWhichAreNotSnapshot(ObjectInstance inObjectInstance) {
        if (inObjectInstance != null) {
            List<ObjectInstanceFile> result = new ArrayList<ObjectInstanceFile>();
            List<ObjectInstanceFile> list_OIFile = this.getListInstanceFileFromObjectInstance(inObjectInstance);
            if (list_OIFile != null) {
                for (ObjectInstanceFile objectInstanceFile : list_OIFile) {
                    if (!objectInstanceFile.getFile().getType().getKeyword().equals("SNAPSHOT")) {
                        result.add(objectInstanceFile);
                    }
                }
            }
            return result;
        } else {
            return null;
        }
    }

    public List<ObjectInstanceFile> getListInstanceFileFromObjectInstance(ObjectInstance OInstance) {
        if (OInstance != null) {
            List<ObjectInstanceFile> result = new ArrayList<ObjectInstanceFile>();
            result = ObjectInstanceFile.getObjectInstanceFileFiltered(OInstance, null, null, "creator", null);
            return result;
        } else {
            return null;
        }
    }

    public void displayFileOnScreen(ObjectInstanceFile inObjectInstanceFile, boolean download) {
        if (inObjectInstanceFile != null) {
            String filepath = inObjectInstanceFile.getFileAbsolutePath();
            net.ihe.gazelle.common.util.DocumentFileUpload.showFile(filepath, inObjectInstanceFile.getUrl(), download);
        }
    }

    public String getLink(ObjectInstanceFile inObjectInstanceFile) {
        return ApplicationPreferenceManagerImpl.instance().getApplicationUrl() + "sampleSnapshot/original/" + inObjectInstanceFile.getId()
                + "/" + inObjectInstanceFile.getUrl();
    }


    public String getThumbnailLink(ObjectInstanceFile inObjectInstanceFile) {
        return ApplicationPreferenceManagerImpl.instance().getApplicationUrl() + "sampleSnapshot/thumbnail/" + inObjectInstanceFile.getId()
                + "/" + inObjectInstanceFile.getUrl();
    }


    public void displayXmlOnScreen(ObjectInstanceFile inObjectInstanceFile) {
        String filepath = inObjectInstanceFile.getFileAbsolutePath();
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("text/xml");
            response.setHeader("Content-Disposition", "attachment;filename=\"file-" + inObjectInstanceFile.getUrl()
                    + ".xml\"");
            // response.sendRedirect("_blank");
            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            AttributeList list = new AttributeList();
            list.read(new File(filepath));
            XMLRepresentationOfDicomObjectFactory.createDocumentAndWriteIt(list, servletOutputStream);
            servletOutputStream.flush();
            servletOutputStream.close();
            context.responseComplete();
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to display file : " + e.getMessage());
            log.error("", e);
        }
    }

    public List<ObjectInstanceFile> getListObjectInstanceFileWhichAreSnapshot(ObjectInstance inObjectInstance) {
        // *
        if (inObjectInstance != null) {
            List<ObjectInstanceFile> result = new ArrayList<ObjectInstanceFile>();
            result = ObjectInstanceFile.getObjectInstanceFileFiltered(inObjectInstance, null, null, "creator",
                    "SNAPSHOT");
            return result;
        } else {
            return null;
        }
    }

    public java.io.File getSnapshotThumbnailFile(ObjectInstanceFile inObjectInstanceFile) {
        if (inObjectInstanceFile != null) {
            String linkpath = "";
            linkpath = inObjectInstanceFile.getFileAbsolutePath();
            String thumbnailPath = ThumbnailCreator.createNewFileName(linkpath);
            java.io.File result = new java.io.File(thumbnailPath);
            if (result.exists()) {
                return result;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * return list of SIS that can read this object type
     */
    private List<SystemInSession> getListOfSystemInSessionReaderForObjectType(ObjectType objectType, TestingSession selectedTestingSession) {
        List<SystemInSession> result = new ArrayList<SystemInSession>();
        if (objectType != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            result = SystemInSession.getSystemInSessionFiltered(em, null, selectedTestingSession, null, null, null, null, null,
                    null, null, null, null, null, null, null, objectType, null, null);
        }
        return result;
    }

    protected List<Annotation> getReadersDescriptions(ObjectInstance inObjectInstance) {
        List<Annotation> result = new ArrayList<Annotation>();
        List<ObjectInstanceAnnotation> list_OIA = ObjectInstanceAnnotation.getObjectInstanceAnnotationFiltered(
                inObjectInstance, null);
        for (ObjectInstanceAnnotation inObjectInstanceAnnotation : list_OIA) {
            result.add(inObjectInstanceAnnotation.getAnnotation());
        }
        return result;
    }

    public List<ObjectInstanceAttribute> getListInstanceAttributeFromObjectInstance(ObjectInstance inOInstance) {
        if (inOInstance != null) {
            return ObjectInstanceAttribute.getObjectInstanceAttributeFiltered(inOInstance);
        } else {
            return null;
        }
    }

    protected String getPermanentLink(ObjectInstance inObjectInstance) {
        String result = "";
        if (inObjectInstance != null) {
            if (inObjectInstance.getId() != null) {
                result = ApplicationPreferenceManagerImpl.instance().getApplicationUrl() + "objects/sample.seam?id="
                        + inObjectInstance.getId().toString();
            }
        }
        return result;
    }

    public List<ObjectInstanceValidation> getPossibleValidationOptions() {
        return ObjectInstanceValidation.getListObjectInstanceValidation();
    }

    public void mergeTheNewValidationOfSample(ObjectInstance inObjectInstance) {
        if (inObjectInstance != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            inObjectInstance = entityManager.merge(inObjectInstance);
            entityManager.flush();
            if (inObjectInstance.getValidation() != null) {
                String comment = "";
                comment = Identity.instance().getCredentials().getUsername()
                        + " changes the validation of this sample to " + inObjectInstance.getValidation().getValue();
                this.saveCommentOfObjectInstance(comment, inObjectInstance);
            } else {
                this.saveCommentOfObjectInstance(Identity.instance().getCredentials().getUsername()
                        + " reset the validation of this sample", inObjectInstance);
            }
        }
    }

    public void mergeObjectInstance(ObjectInstance inObjectInstance) {
        if (inObjectInstance != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            inObjectInstance = entityManager.merge(inObjectInstance);
            entityManager.flush();
        }
    }

    protected void saveCommentOfObjectInstance(String commentt, ObjectInstance oi) {
        if (commentt != null) {
            if (commentt.length() > 499) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "#{messages['gazelle.testmanagement.object.CommentSampleLong']}");
            } else {
                // String comment = StringEscapeUtils.escapeHtml(commentt);
                Annotation inAnnotation = new Annotation(commentt);
                EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
                inAnnotation = entityManager.merge(inAnnotation);
                entityManager.flush();
                ObjectInstanceAnnotation inObjectInstanceAnnotation = new ObjectInstanceAnnotation(oi, inAnnotation);
                inObjectInstanceAnnotation = entityManager.merge(inObjectInstanceAnnotation);
                entityManager.flush();
                commentt = null;
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "#{messages['gazelle.testmanagement.object.yourcommentwassaved']}");
            }
        }
    }

    // trees //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * tree of objects added by readers
     */
    public GazelleTreeNodeImpl<Object> getTreeNodeOfReaderInstanceFile(ObjectInstance inObjectInstance, TestingSession selectedTestingSession) {
        GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();
        if (inObjectInstance != null) {
            List<SystemInSession> list_SISReader = this.getListOfSystemInSessionReaderForObjectType(inObjectInstance
                    .getObject(), selectedTestingSession);
            Collections.sort(list_SISReader);
            int i = 0;
            int j;
            for (SystemInSession inSystemInSession : list_SISReader) {
                GazelleTreeNodeImpl<Object> SISTreeNode = new GazelleTreeNodeImpl<Object>();
                SISTreeNode.setData(inSystemInSession);
                List<ObjectInstanceFile> list_OIF = ObjectInstanceFile.getObjectInstanceFileFiltered(inObjectInstance,
                        null, inSystemInSession, "reader", null);
                Collections.sort(list_OIF);
                j = 0;
                for (ObjectInstanceFile inObjectInstanceFile : list_OIF) {
                    GazelleTreeNodeImpl<Object> OIFTreeNode = new GazelleTreeNodeImpl<Object>();
                    OIFTreeNode.setData(inObjectInstanceFile);
                    SISTreeNode.addChild(j, OIFTreeNode);
                    j++;
                }

                rootNode.addChild(i, SISTreeNode);
                i++;
            }
        }
        return rootNode;
    }

    @SuppressWarnings("rawtypes")
    public TreeNode getTreeNodeOfDicomObjectInstanceFile(ObjectInstanceFile inObjectInstanceFile) {
        if (inObjectInstanceFile != null) {
            String filePath = "";
            filePath = inObjectInstanceFile.getFileAbsolutePath();
            AttributeList list = new AttributeList();
            try {
                list.read(new File(filePath));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                XMLRepresentationOfDicomObjectFactory.createDocumentAndWriteIt(list, baos);
                return XmlTreeDataBuilder.build(new InputSource(new ByteArrayInputStream(baos.toByteArray())));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            } catch (DicomException e) {
                log.error(e.getMessage(), e);
            } catch (SAXException e) {
                log.error(e.getMessage(), e);
            }
            return null;
        } else {
            return null;
        }
    }

    public boolean canEditSampleByCreator(ObjectInstance selectedObjectInstance, TestingSession selectedTestingSession) {
        if (selectedObjectInstance != null) {
            List<ObjectInstanceFile> loif = ObjectInstanceFile.getObjectInstanceFileFiltered(selectedObjectInstance,
                    null, null, "reader", null);
            if (loif != null) {
                if (loif.size() > 0) {
                    return false;
                }
            }
        }
        if (selectedTestingSession.testingSessionClosedForUser()) {
            return false;
        }
        return true;
    }

    public Boolean getSelectedObjectInstanceExistsOnDisc(ObjectInstance selectedObjectInstance) {
        Boolean selectedObjectInstanceExistsOnDisc = false;
        if (selectedObjectInstance != null) {
            boolean noprob = true;
            if (selectedObjectInstance.getObjectInstanceFiles() != null) {
                for (ObjectInstanceFile oif : selectedObjectInstance.getObjectInstanceFiles()) {
                    String path = oif.getFileAbsolutePath();
                    File fil = new File(path);
                    if (!fil.exists()) {
                        noprob = false;
                        break;
                    }
                }
            }
            if (noprob) {
                selectedObjectInstanceExistsOnDisc = true;
            }
        }
        return selectedObjectInstanceExistsOnDisc;
    }

}
