/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.utils.systems;

import java.util.Comparator;

public class IHEImplementationForSystemComparatorIntegrationProfile implements Comparator<IHEImplementationForSystem> {

	@Override
	public int compare(IHEImplementationForSystem o1, IHEImplementationForSystem o2) {
		if (o1.getIntegrationProfile().getKeyword().compareTo(o2.getIntegrationProfile().getKeyword()) < 0) {
			return -1;
		} else if (o1.getIntegrationProfile().getKeyword().compareTo(o2.getIntegrationProfile().getKeyword()) > 0) {
			return 1;
		} else {
			if (o1.getActor().getKeyword().compareTo(o2.getActor().getKeyword()) <= 0) {
				return -1;
			} else {
				return 1;
			}
		}
	}

}
