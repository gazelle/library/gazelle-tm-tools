package net.ihe.gazelle.tm.utils;

import java.awt.image.BufferedImage;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import net.ihe.gazelle.common.filecache.FileCacheRenderer;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

public class QRCodeRenderer implements FileCacheRenderer {

	public static final FileCacheRenderer SINGLETON = new QRCodeRenderer();

	private static final int BLACK = 0xFF000000;
	private static final int WHITE = 0x00FFFFFF;

	@Override
	public void render(OutputStream out, String value) throws Exception {
		QRCode code = new QRCode();
		Encoder.encode(value, ErrorCorrectionLevel.L, null, code);
		ByteMatrix input = code.getMatrix();
		int inputWidth = input.getWidth();
		int inputHeight = input.getHeight();
		int multiple = Math.min(200 / inputWidth, 200 / inputHeight);

		BufferedImage image = new BufferedImage(inputWidth * multiple, inputHeight * multiple,
				BufferedImage.TYPE_INT_ARGB);

		for (int i = 0; i < inputWidth; i++) {
			for (int j = 0; j < inputHeight; j++) {
				int color = input.get(i, j) == 1 ? BLACK : WHITE;
				for (int k = 0; k < multiple; k++) {
					for (int k2 = 0; k2 < multiple; k2++) {
						image.setRGB((i * multiple) + k, (j * multiple) + k2, color);
					}
				}
			}
		}

		ImageIO.write(image, "png", out);
	}

	@Override
	public String getContentType() {
		return "image/png";
	}

}
