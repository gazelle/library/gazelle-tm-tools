/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.report.action;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.users.model.Institution;
import net.sf.jasperreports.engine.JRException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.navigation.Pages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import static org.jboss.seam.ScopeType.SESSION;


@Name("reportManagerBean")
@Scope(SESSION)
@GenerateInterface("ReportManagerLocal")
public class ReportManager implements Serializable, ReportManagerLocal {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450921436961253710L;

    private static final Logger log = LoggerFactory.getLogger(ReportManager.class);

    @In(create = true, value = "gumUserService")
    private transient UserService userService;

    @Override
    public void listTransactionsToSupportForGivenSystemPDF(TestingSession selectedTestingSession) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("sessionId", selectedTestingSession.getId());
        try {
            ReportExporterManager.exportToPDF("listTransactionsToSupportForGivenSystem",
                    "listTransactionsToSupportForGivenSystem.pdf", parameters);

        } catch (JRException e) {
            ExceptionLogging.logException(e, log);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        }
    }

    @Override
    public void listOfPreconnectathonTestsPDF(SystemInSession currentSystemInSession) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("systemId", currentSystemInSession.getSystem().getId());
        try {
            ReportExporterManager.exportToPDF("listPreconnectathonTestPerSystem", "listPreconnectathonTestFor"
                    + currentSystemInSession.getSystem().getKeyword() + ".pdf", parameters);
            log.debug("Report listPreconnectathonTestPerSystem created for system "
                    + currentSystemInSession.getSystem().getKeyword());
        } catch (JRException e) {
            ExceptionLogging.logException(e, log);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        }
    }

    @Override
    public void listSystemsForAllActorIntegrationProfilePDF(TestingSession selectedTestingSession) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("testingSessionId", selectedTestingSession.getId());
        try {
            ReportExporterManager.exportToPDF("listSystemsForAllActorIntegrationProfile",
                    "listSystemsForAllActorIntegrationProfile.pdf", parameters);

        } catch (JRException e) {
            ExceptionLogging.logException(e, log);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
        }

    }

    @Override
    public void listSystemsSummary(TestingSession selectedTestingSession) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();

        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());

        HSSFSheet worksheet = workbook.createSheet("Systems summary");
        worksheet.createFreezePane(0, 1);

        TestingSession activeSession = selectedTestingSession;
        List<Demonstration> listOfDemonstration = Demonstration.getActiveDemonstrationsForSession(activeSession);

        Collection<IntegrationProfile> listOfIntegrationProfile = activeSession.getIntegrationProfilesUnsorted();
        listOfIntegrationProfile = Collections2.filter(listOfIntegrationProfile, new Predicate<IntegrationProfile>() {
            @Override
            public boolean apply(IntegrationProfile input) {
                if ((input != null) && (input.getIntegrationProfileStatusType() != null)) {
                    String keyword = input.getIntegrationProfileStatusType().getKeyword();
                    if ("TI".equals(keyword) || "FT".equals(keyword)) {
                        return true;
                    }
                }
                return false;
            }
        });

        int colIndex = 0;
        int rowIndex = 0;
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Organization name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Organization keyword");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Primary Tech Contact Last Name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Primary Tech Contact First Name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Primary Tech Contact email");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("System");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("System Status");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("System is accepted to session");

        if (listOfDemonstration != null) {
            for (Demonstration demonstration : listOfDemonstration) {
                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(demonstration.getName());
            }
        }
        for (IntegrationProfile ip : listOfIntegrationProfile) {
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(ip.getKeyword());
        }

        rowIndex++;

        List<Institution> listOfInstitution = TestingSession.getListOfInstitutionsParticipatingInSession(activeSession);

        for (Institution currentInstitution : listOfInstitution) {
            List<System> listOfSystemForCurrentInstitution = System.getSystemsForAnInstitution(currentInstitution);
            for (System systemInInstitution : listOfSystemForCurrentInstitution) {
                SystemInSession sis = SystemInSession.getSystemInSessionForSession(systemInInstitution, activeSession);
                if (sis != null) {
                    colIndex = 0;
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(currentInstitution.getName());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(currentInstitution.getKeyword());
                    int colBackupIndex = colIndex;
                    try {
                        User user = userService.getUserById(systemInInstitution.getOwnerUserId());
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue(user.getLastName());
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue(user.getFirstName());
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue(user.getEmail());
                    } catch (NoSuchElementException e) {
                        colIndex = colBackupIndex;
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue(systemInInstitution.getOwnerUserId());
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue(systemInInstitution.getOwnerUserId());
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("");
                    }
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(systemInInstitution.getKeyword());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(
                            sis.getRegistrationStatus() != null ? sis.getRegistrationStatus().toString() : "");
                    if (sis.getAcceptedToSession() != null && sis.getAcceptedToSession()) {
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("true");
                    } else {
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("false");
                    }
                    if (listOfDemonstration != null) {
                        for (Demonstration demonstration : listOfDemonstration) {
                            if ((sis.getDemonstrations() != null) && sis.getDemonstrations().contains(demonstration)) {
                                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(demonstration.getName());
                            } else {
                                getCell(worksheet, style, rowIndex, colIndex++).setCellValue("");
                            }
                        }
                    }
                    List<IntegrationProfile> listOfIntegrationProfileForThisSystem = SystemActorProfiles
                            .getListOfIntegrationProfilesImplementedByASystem(systemInInstitution);
                    for (IntegrationProfile ip : listOfIntegrationProfile) {
                        if (listOfIntegrationProfileForThisSystem.contains(ip)) {
                            List<Actor> actors = SystemActorProfiles
                                    .getListOfActorsImplementedByASystemForAnIntegrationProfile(systemInInstitution, ip);
                            StringBuilder sb = new StringBuilder("");
                            for (int i = 0; i < actors.size(); i++) {
                                sb.append(actors.get(i).getKeyword());
                                if (i < (actors.size() - 1)) {
                                    sb.append(";");
                                }
                            }
                            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(sb.toString());
                        } else {
                            getCell(worksheet, style, rowIndex, colIndex++).setCellValue("");
                        }
                    }

                    rowIndex++;
                }

            }
        }

        for (int columnIndex = 0; columnIndex < colIndex; columnIndex++) {
            worksheet.autoSizeColumn(columnIndex);
        }

        CellReference start = new CellReference(0, 0);
        String startString = start.formatAsString();
        CellReference end = new CellReference(rowIndex - 1, colIndex - 1);
        String endString = end.formatAsString();
        worksheet.setAutoFilter(CellRangeAddress.valueOf(startString + ":" + endString));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        Pages.getCurrentBaseName();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=\"systemsSummary.xls\"");
        workbook.write(servletOutputStream);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

    @Override
    public void listSystemsSummary(List<IntegrationProfile> listOfIntegrationProfile, List<System> systemsList,
                                   TestingSession selectedTestingSession) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();

        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());

        HSSFSheet worksheet = workbook.createSheet("Systems summary");
        worksheet.createFreezePane(0, 1);

        List<Demonstration> listOfDemonstration = Demonstration.getActiveDemonstrationsForSession(selectedTestingSession);

        Collection<IntegrationProfile> ipList = Collections2.filter(listOfIntegrationProfile, new Predicate<IntegrationProfile>() {
            @Override
            public boolean apply(IntegrationProfile input) {
                if ((input != null) && (input.getIntegrationProfileStatusType() != null)) {
                    String keyword = input.getIntegrationProfileStatusType().getKeyword();
                    if ("TI".equals(keyword) || "FT".equals(keyword)) {
                        return true;
                    }
                }
                return false;
            }
        });
        listOfIntegrationProfile = new ArrayList<>(ipList);

        int colIndex = 0;
        int rowIndex = 0;
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Organization name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Organization keyword");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Primary Tech Contact Last Name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Primary Tech Contact First Name");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("Primary Tech Contact email");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("System");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("System Status");
        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("System is accepted to session");

        if (listOfDemonstration != null) {
            for (Demonstration demonstration : listOfDemonstration) {
                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(demonstration.getName());
            }
        }
        for (IntegrationProfile ip : listOfIntegrationProfile) {
            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(ip.getKeyword());
        }

        rowIndex++;
        for (System systemInInstitution : systemsList) {
            SystemInSession sis = SystemInSession.getSystemInSessionForSession(systemInInstitution, selectedTestingSession);
            if (sis != null) {
                Institution currentInstitution = sis.getSystem().getUniqueInstitution();
                colIndex = 0;
                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(currentInstitution.getName());
                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(currentInstitution.getKeyword());
                int colBackupIndex = colIndex;
                try {
                    User user = userService.getUserById(systemInInstitution.getOwnerUserId());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(user.getLastName());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(user.getFirstName());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(user.getEmail());
                } catch (NoSuchElementException e) {
                    colIndex = colBackupIndex;
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(systemInInstitution.getOwnerUserId());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue(systemInInstitution.getOwnerUserId());
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue("");
                }
                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(systemInInstitution.getKeyword());
                getCell(worksheet, style, rowIndex, colIndex++).setCellValue(
                        sis.getRegistrationStatus() != null ? sis.getRegistrationStatus().toString() : "");
                if (sis.getAcceptedToSession() != null && sis.getAcceptedToSession()) {
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue("true");
                } else {
                    getCell(worksheet, style, rowIndex, colIndex++).setCellValue("false");
                }
                if (listOfDemonstration != null) {
                    for (Demonstration demonstration : listOfDemonstration) {
                        if ((sis.getDemonstrations() != null) && sis.getDemonstrations().contains(demonstration)) {
                            getCell(worksheet, style, rowIndex, colIndex++).setCellValue(demonstration.getName());
                        } else {
                            getCell(worksheet, style, rowIndex, colIndex++).setCellValue("");
                        }
                    }
                }
                List<IntegrationProfile> listOfIntegrationProfileForThisSystem = SystemActorProfiles
                        .getListOfIntegrationProfilesImplementedByASystem(systemInInstitution);
                for (IntegrationProfile ip : listOfIntegrationProfile) {
                    if (listOfIntegrationProfileForThisSystem.contains(ip)) {
                        List<Actor> actors = SystemActorProfiles
                                .getListOfActorsImplementedByASystemForAnIntegrationProfile(systemInInstitution, ip);
                        StringBuilder sb = new StringBuilder("");
                        for (int i = 0; i < actors.size(); i++) {
                            sb.append(actors.get(i).getKeyword());
                            if (i < (actors.size() - 1)) {
                                sb.append(";");
                            }
                        }
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue(sb.toString());
                    } else {
                        getCell(worksheet, style, rowIndex, colIndex++).setCellValue("");
                    }
                }

                rowIndex++;
            }

        }

        for (int columnIndex = 0; columnIndex < colIndex; columnIndex++) {
            worksheet.autoSizeColumn(columnIndex);
        }

        CellReference start = new CellReference(0, 0);
        String startString = start.formatAsString();
        CellReference end = new CellReference(rowIndex - 1, colIndex - 1);
        String endString = end.formatAsString();
        worksheet.setAutoFilter(CellRangeAddress.valueOf(startString + ":" + endString));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        Pages.getCurrentBaseName();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=\"systemsSummary.xls\"");
        workbook.write(servletOutputStream);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

    private HSSFCell getCell(HSSFSheet worksheet, CellStyle style, int irow, int icol) {
        HSSFRow row = worksheet.getRow(irow);
        if (row == null) {
            row = worksheet.createRow(irow);
            row.setRowStyle(style);
        }
        HSSFCell cell = row.getCell(icol);
        if (cell == null) {
            cell = row.createCell(icol);
        }
        return cell;
    }

    /**
     * Destroy the Manager bean when the session is over.
     */
    @Override
    @Destroy
    @Remove
    public void destroy() {

    }
}
