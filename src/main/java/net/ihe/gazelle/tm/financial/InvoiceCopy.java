package net.ihe.gazelle.tm.financial;

import java.util.Date;

import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

public class InvoiceCopy {

	private String name;

	private String pathName;

	private Institution institution;

	private TestingSession testingSession;

	private Date dateGeneration;

	private String year;

	private String path;

	private String number;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public InvoiceCopy(Institution institution, TestingSession testingSession, Date dateGeneration) {
		super();
		this.institution = institution;
		this.testingSession = testingSession;
		this.dateGeneration = dateGeneration;
	}

	public InvoiceCopy() {
	}

	@Override
	public String toString() {
		return "InvoiceCopy [institution=" + institution + ", testingSession=" + testingSession + ", dateGeneration="
				+ dateGeneration + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((dateGeneration == null) ? 0 : dateGeneration.hashCode());
		result = (prime * result) + ((institution == null) ? 0 : institution.hashCode());
		result = (prime * result) + ((testingSession == null) ? 0 : testingSession.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		InvoiceCopy other = (InvoiceCopy) obj;
		if (dateGeneration == null) {
			if (other.dateGeneration != null) {
				return false;
			}
		} else if (!dateGeneration.equals(other.dateGeneration)) {
			return false;
		}
		if (institution == null) {
			if (other.institution != null) {
				return false;
			}
		} else if (!institution.equals(other.institution)) {
			return false;
		}
		if (testingSession == null) {
			if (other.testingSession != null) {
				return false;
			}
		} else if (!testingSession.equals(other.testingSession)) {
			return false;
		}
		return true;
	}

	public TestingSession getTestingSession() {
		return testingSession;
	}

	public void setTestingSession(TestingSession testingSession) {
		this.testingSession = testingSession;
	}

	public Date getDateGeneration() {
		return dateGeneration;
	}

	public void setDateGeneration(Date dateGeneration) {
		this.dateGeneration = dateGeneration;
	}

}
