package net.ihe.gazelle.tm.gazelletest.checklist;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class NodeTransformer {

	public static Nodes load(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.tm.gazelletest.checklist");
		Unmarshaller um = jc.createUnmarshaller();
		Nodes nodes = (Nodes) um.unmarshal(is);
		return nodes;
	}

	public static void save(OutputStream os, Nodes nds) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.tm.gazelletest.checklist");
		Marshaller mr = jc.createMarshaller();
		mr.marshal(nds, os);
	}

}
