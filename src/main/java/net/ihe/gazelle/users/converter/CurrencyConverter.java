/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.converter;

import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.users.model.Currency;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;
import java.io.Serializable;

@BypassInterceptors
@Name("currencyConverter")
@Converter(forClass = Currency.class)
public class CurrencyConverter implements javax.faces.convert.Converter, Serializable {

    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(CurrencyConverter.class);

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value instanceof Currency) {
            Currency currency = (Currency) value;
            return currency.getKeyword();
        } else {
            return null;
        }

    }

    @Override
    @Transactional
    public Object getAsObject(FacesContext Context, UIComponent Component, String value) throws ConverterException {
        if (value != null) {
            try {
                EntityManager entityManager = (EntityManager) org.jboss.seam.Component.getInstance("entityManager");
                Currency currency = entityManager.find(Currency.class, value);
                return currency;
            } catch (NumberFormatException e) {
                ExceptionLogging.logException(e, log);
            }
        }
        return null;
    }

}
