/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

import net.ihe.gazelle.users.model.Iso3166CountryCode;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@BypassInterceptors
@Name("iso3166CountryCodeConverter")
@Converter(forClass = Iso3166CountryCode.class)
public class Iso3166CountryCodeConverter implements javax.faces.convert.Converter, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4106342620255186526L;

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		if (value instanceof Iso3166CountryCode) {
			Iso3166CountryCode country = (Iso3166CountryCode) value;
			return country.getIso();
		} else {
			return "";
		}

	}

	@Override
	@Transactional
	public Object getAsObject(FacesContext Context, UIComponent Component, String iso) throws ConverterException {
		if (iso != null) {
			EntityManager entityManager = (EntityManager) org.jboss.seam.Component.getInstance("entityManager");
			return entityManager.find(Iso3166CountryCode.class, iso);
		}
		return null;
	}

}
