package net.ihe.gazelle.pr.bean;

import net.ihe.gazelle.tm.systems.model.System;
import org.apache.commons.io.IOExceptionWithCause;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.*;

public class ISTools {
    private static final Logger LOG = LoggerFactory.getLogger(ISTools.class);

    public static void retrieveIS(System system, File tempFile) throws IOException {

        String integrationStatementUrl = system.getIntegrationStatementUrl();
        if (StringUtils.trimToNull(integrationStatementUrl) == null) {
            throw new IOException("Integration statement URL is not set");
        }

        integrationStatementUrl = URLDecoder.decode(integrationStatementUrl, "UTF-8");
        URL url = new URL(integrationStatementUrl);
        URI uri;
        try {
            uri = new URI(url.getProtocol(), url.getAuthority(), url.getPath(), url.getQuery(), url.getRef());
        } catch (URISyntaxException e) {
            throw new IOException("Failed to parse URL " + e.getMessage(), e);
        }
        integrationStatementUrl = uri.toString();
        url = new URL(integrationStatementUrl);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.addRequestProperty("User-Agent", "Mozilla/4.0");
        InputStream content = conn.getInputStream();
        IOUtils.copy(content, new FileOutputStream(tempFile));
        conn.disconnect();
    }

    public static BufferedImage getThumbnailOfPDF(File tempFile) throws Throwable {
        File tempPNG = File.createTempFile("ispng", ".png");
        try {
            String[] commands = new String[]{"convert", "-thumbnail", "119x170", "-alpha", "remove",
                    "-background", "white", tempFile.getAbsolutePath() + "[0]", tempPNG.getAbsolutePath()};
            performCommand(commands, 100, 30000);
            BufferedImage result = ImageIO.read(tempPNG);
            if (!tempPNG.delete()) {
                throw new Exception("Could not delete the temporary file");
            }
            return result;
        } catch (Exception e) {
            if (!tempPNG.delete()) {
                throw new Exception("Could not delete the temporary file", e);
            }
            throw e;
        }
    }

    // -----------------------------------------------------------------------

    /**
     * Performs the os command.
     *
     * @param cmdAttribs the command line parameters
     * @param max        The maximum limit for the lines returned
     * @param timeout    The timout amount in milliseconds or no timeout if the value is zero or less
     * @return the parsed data
     * @throws IOException if an error occurs
     */
    static List<String> performCommand(String[] cmdAttribs, int max, long timeout) throws IOException {
        // this method does what it can to avoid the 'Too many open files' error
        // based on trial and error and these links:
        // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4784692
        // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4801027
        // http://forum.java.sun.com/thread.jspa?threadID=533029&messageID=2572018
        // however, its still not perfect as the JDK support is so poor
        // (see commond-exec or ant for a better multi-threaded multi-os
        // solution)

        List<String> lines = new ArrayList<String>(20);
        Process proc = null;
        InputStream in = null;
        OutputStream out = null;
        InputStream err = null;
        BufferedReader inr = null;
        try {

            Thread monitor = ThreadMonitor.start(timeout);

            proc = openProcess(cmdAttribs);
            in = proc.getInputStream();
            out = proc.getOutputStream();
            err = proc.getErrorStream();
            inr = new BufferedReader(new InputStreamReader(in));
            String line = inr.readLine();
            while ((line != null) && (lines.size() < max)) {
                line = line.toLowerCase(Locale.ENGLISH).trim();
                lines.add(line);
                line = inr.readLine();
            }

            proc.waitFor();

            ThreadMonitor.stop(monitor);

            if (proc.exitValue() != 0) {
                // os command problem, throw exception
                throw new IOException("Command line returned OS error code '" + proc.exitValue() + "' for command "
                        + Arrays.asList(cmdAttribs));
            }
            return lines;

        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            throw new IOExceptionWithCause("Command line threw an InterruptedException for command "
                    + Arrays.asList(cmdAttribs) + " timeout=" + timeout, ex);
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(err);
            IOUtils.closeQuietly(inr);
            if (proc != null) {
                proc.destroy();
            }
        }
    }

    /**
     * Opens the process to the operating system.
     *
     * @param cmdAttribs the command line parameters
     * @return the process
     * @throws IOException if an error occurs
     */
    static Process openProcess(String[] cmdAttribs) throws IOException {
        return Runtime.getRuntime().exec(cmdAttribs);
    }
}
