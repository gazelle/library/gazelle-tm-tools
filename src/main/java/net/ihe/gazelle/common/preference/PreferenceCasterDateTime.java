package net.ihe.gazelle.common.preference;

import java.util.Date;

public class PreferenceCasterDateTime implements PreferenceCaster<Date> {

	@Override
	public Date asObject(String stringValue) {
		if (stringValue == null) {
			return new Date();
		}
		try {
			Long date = Long.valueOf(stringValue);
			return new Date(date);
		} catch (NumberFormatException e) {
			return new Date();
		}
	}

	@Override
	public String asString(Date value) {
		if (value == null) {
			return "0";
		}
		return Long.toString(value.getTime());
	}

	@Override
	public String getPreferenceClassName() {
		return Date.class.getCanonicalName();
	}

}
