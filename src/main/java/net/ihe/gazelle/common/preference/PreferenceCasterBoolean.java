/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.preference;

class PreferenceCasterBoolean implements PreferenceCaster<Boolean> {

	private static final String FALSE = "false";

	private static final String TRUE = "true";

	@Override
	public String asString(Boolean value) {
		if (value == null) {
			return FALSE;
		}
		if (!value) {
			return FALSE;
		}
		return TRUE;
	}

	@Override
	public Boolean asObject(String stringValue) {
		if (stringValue == null) {
			return Boolean.FALSE;
		}
		String upperCase = stringValue.toLowerCase();
		if (upperCase.equals(TRUE)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public String getPreferenceClassName() {
		return Boolean.class.getCanonicalName();
	}
}