/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.preference;

public enum PreferenceType implements
		PreferenceCaster<Object> {

	STRING(new PreferenceCasterString()),

	BOOLEAN(new PreferenceCasterBoolean()),

	BIGDECIMAL(new PreferenceCasterBigDecimal()),

	INTEGER(new PreferenceCasterInteger()),

	DATETIME(new PreferenceCasterDateTime());

	private PreferenceCaster<Object> delegator;

	<T extends Object> PreferenceType(PreferenceCaster<T> delegator) {
		this.delegator = (PreferenceCaster<Object>) delegator;
	}

	@Override
	public Object asObject(String stringValue) {
		return delegator.asObject(stringValue);
	}

	@Override
	public String asString(Object value) {
		return delegator.asString(value);
	}

	@Override
	public String getPreferenceClassName() {
		return delegator.getPreferenceClassName();
	}

}
