package net.ihe.gazelle.common.util;

import org.apache.commons.lang.ArrayUtils;
import org.richfaces.model.UploadedFile;

import javax.activation.FileTypeMap;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

/**
 * <b>Class Description : </b>File<br>
 * <br>
 * This class describes a File object used during upload actions. This is a way to upload a Document/File on a Gazelle server. It corresponds to the Business Layer. It is used by all Richfaces
 * listener components : <rich:fileUpload ... >
 *
 * @author Jean-Renan Chatel - Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 4th
 * @class File.java
 * @package net.ihe.gazelle.common.util
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
public class File implements Serializable {

	private static final long serialVersionUID = -4706278516271245642L;

	private String name;

	private byte[] data;

	public File() {
		super();
	}

	public File(String name, byte[] data) {
		super();
		setName(name);
		this.data = data;
	}

	public File(UploadedFile item) {
		super();
		setName(item.getName());
		setData(item.getData());
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = ArrayUtils.clone(data);
	}

	public String getName() {
		return name;
	}

	private void setName(String completename) {
		int indexOfSeparator = completename.lastIndexOf(java.io.File.separatorChar);
		if (indexOfSeparator >= 0) {
			name = completename.substring(indexOfSeparator + 1);
		} else {
			name = completename;
		}
	}

	public long getLength() {
		return data.length;
	}

	public String getMime() {
		return FileTypeMap.getDefaultFileTypeMap().getContentType(name);
	}

	public InputStream getInputStream() {
		return new ByteArrayInputStream(data);
	}

}
