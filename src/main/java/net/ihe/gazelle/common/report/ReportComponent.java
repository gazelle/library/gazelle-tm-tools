package net.ihe.gazelle.common.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.el.ValueExpression;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReportComponent extends UIComponentBase {

	private static Logger log = LoggerFactory.getLogger(ReportComponent.class);
	/** Filename of the report. */
	private String report;
	/** The report's parameters. */
	private Map<String, Object> parameters = new HashMap<String, Object>();

	public ReportComponent() {

	}

	@Override
	public String getFamily() {
		return "Jasper";
	}

	/** Returns the name of the report (which may include a path). */
	public String getReport() {
		if (report == null) {
			ValueExpression ve = getValueExpression("report");
			if (ve != null) {
				report = (String) ve.getValue(getFacesContext().getELContext());
			}
		}
		return report;
	}

	public void setReport(String report) {
		this.report = report;

	}

	/** Add a new parameter to the report. */
	public void addParameter(String name, Object value) {
		parameters.put(name, value);
	}

	/** Get the map of report parameters. */
	public Map<String, Object> getParameters() {
		return parameters;
	}

	/**
	 * <p>
	 * Return the state to be saved for this component.
	 * </p>
	 *
	 * @param context
	 *            <code>FacesContext</code> for the current request
	 */
	@Override
	public Object saveState(FacesContext context) {
		Object[] values = new Object[3];
		values[0] = super.saveState(context);
		values[1] = report;
		values[2] = parameters;

		return (values);
	}

	/**
	 * <p>
	 * Restore the state for this component.
	 * </p>
	 *
	 * @param context
	 *            <code>FacesContext</code> for the current request
	 * @param state
	 *            State to be restored
	 *
	 * @throws IOException
	 *             if an input/output error occurs
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void restoreState(FacesContext context, Object state) {
		Object[] values = (Object[]) state;
		super.restoreState(context, values[0]);
		report = (String) values[1];
		parameters = (Map<String, Object>) values[2];
	}

	@Override
	public String getRendererType() {
		return "Report";
	}

}