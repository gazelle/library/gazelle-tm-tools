/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *    Author : eric.poiseau@inria.fr INRIA
 */
package net.ihe.gazelle.common.session;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import net.ihe.gazelle.common.profiling.TimingInterceptor;
import net.ihe.gazelle.common.session.GazelleSessionListener.HttpSessionUser;
import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This filter is used to detect robots, manage activation and change password redirections after first login.
 * This filter also create a cookie to identify the browser.
 */
// FIXME : this class should be split in two filters (RobotFilter [gazelle-tools], ActivationFilter [gazelle-tm])
public class RobotsAndActivationFilter implements Filter {

    private static final String TIMEOUT_PAGE = "home.seam";
    private static final String USER_REGISTRATION = "firstUserAfterLoggedOut.seam?activationCode=";
    private static final String CHANGE_PASSWORD = "changeIHEPassword.seam?changePasswordCode=";
    private static final String IS_ROBOT = "isRobot";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //comment for sonar
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException,
            ServletException {
        if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;

            HttpSession session = httpServletRequest.getSession();
            Boolean isRobot = (Boolean) session.getAttribute(IS_ROBOT);
            try {
                if (isRobot == null) {
                    synchronized (session) {
                        isRobot = (Boolean) session.getAttribute(IS_ROBOT);
                        if (isRobot == null) {
                            String browserInfo = httpServletRequest.getHeader("user-agent");
                            if (browserInfo != null) {
                                UserAgent userAgent = UserAgent.parseUserAgentString(browserInfo);
                                if (userAgent.getBrowser() == Browser.BOT) {
                                    isRobot = Boolean.TRUE;
                                } else {
                                    isRobot = Boolean.FALSE;
                                }
                                session.setAttribute(IS_ROBOT, isRobot);
                            } else {
                                isRobot = Boolean.FALSE;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                isRobot = Boolean.FALSE;
            }
            if (isRobot) {
                httpServletResponse.sendError(503, "Check robots.txt");
                return;
            }
            HttpSessionUser httpSessionUser = GazelleSessionListener.getSession(session.getId());
            httpSessionUser.init(httpServletRequest);

            // is session expire control required for this request?
            if (isSessionControlRequiredForThisResource(httpServletRequest)) {
                // is session invalid?
                if (isSessionInvalid(httpServletRequest)) {
                    String activationCode_ = httpServletRequest.getParameter("activationCode");
                    String changePasswordCode_ = httpServletRequest.getParameter("changePasswordCode");
                    if ((activationCode_ != null)
                            && !activationCode_.equals("")
                            && (httpServletRequest.getRequestURI().indexOf(
                            USER_REGISTRATION.substring(0, USER_REGISTRATION.indexOf('?'))) == -1)) {
                        String userRegistrationUrl = httpServletRequest.getContextPath() + "/" + USER_REGISTRATION
                                + activationCode_;
                        httpServletResponse.sendRedirect(userRegistrationUrl);
                        return;
                    } else if ((changePasswordCode_ != null)
                            && !changePasswordCode_.equals("")
                            && (httpServletRequest.getRequestURI().indexOf(
                            CHANGE_PASSWORD.substring(0, CHANGE_PASSWORD.indexOf('?'))) == -1)) {
                        String userRegistrationUrl = httpServletRequest.getContextPath() + "/" + CHANGE_PASSWORD
                                + changePasswordCode_;
                        httpServletResponse.sendRedirect(userRegistrationUrl);
                        return;
                    }
                }
            }
        }
        //FIXME : to remove
        TimingInterceptor.CALL_CHAIN.get().clear();

        filterChain.doFilter(request, response);
    }

	/*
     * session shouldn't be checked for some pages. For example: for timeout
	 * page.. Since we're redirecting to timeout page from this filter, if we
	 * don't disable session control for it, filter will again redirect to it and
	 * this will be result with an infinite loop...
	 */

    private boolean isSessionControlRequiredForThisResource(HttpServletRequest httpServletRequest) {
        String requestPath = httpServletRequest.getRequestURI();
        boolean controlRequired = !StringUtils.contains(requestPath, TIMEOUT_PAGE);
        return controlRequired;
    }

    private boolean isSessionInvalid(HttpServletRequest httpServletRequest) {
        boolean sessionInValid = (httpServletRequest.getRequestedSessionId() != null)
                && !httpServletRequest.isRequestedSessionIdValid();
        return sessionInValid;
    }

    @Override
    public void destroy() {
    }
}
