/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.common.application.action;

import net.ihe.gazelle.cache.keyvalue.KeyValueCacheManager;
import net.ihe.gazelle.cache.keyvalue.MapDbCache;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>ApplicationCacheManager<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 01/02/16
 * @class ApplicationCacheManager
 * @package net.ihe.gazelle.cache
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Name("applicationCacheManager")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationCacheManagerLocal")
public class ApplicationCacheManager implements ApplicationCacheManagerLocal {
    private static final KeyValueCacheManager webServicesCache = new KeyValueCacheManager(new MapDbCache(5));
    private static final KeyValueCacheManager applicationPreferencesCache = new KeyValueCacheManager(new MapDbCache(60));

    private static List<CacheHolder> caches;

    public ApplicationCacheManager() {
        createCache();
    }

    public static List<CacheHolder> getCaches() {
        return caches;
    }

    public static KeyValueCacheManager getApplicationPreferencesCache() {
        return applicationPreferencesCache;
    }

    public static KeyValueCacheManager getWebServicesCache() {
        return webServicesCache;
    }

    private synchronized void createCache() {
        if (caches == null) {
            caches = new ArrayList<>();

            caches.add(new CacheHolder(webServicesCache, "WebServicesCache"));
            caches.add(new CacheHolder(applicationPreferencesCache, "ApplicationPreferencesCache"));
        }
    }

    @Override
    public void enableCaches() {
        webServicesCache.enable();
        applicationPreferencesCache.enable();
    }

    @Override
    public void disableCaches() {
        webServicesCache.disable();
        applicationPreferencesCache.disable();
    }

    @Override
    public void clearCaches() {
        webServicesCache.clearCache();
        applicationPreferencesCache.clearCache();
    }

    @Override
    public boolean isCacheEnabled(KeyValueCacheManager cache) {
        return cache.isEnable();
    }

    @Override
    public void disableCache(KeyValueCacheManager cache) {
        cache.disable();
    }

    @Override
    public void enableCache(KeyValueCacheManager cache) {
        cache.enable();
    }

    @Override
    public void clearCache(KeyValueCacheManager cache) {
        cache.clearCache();
    }

    private static class CacheHolder {
        private KeyValueCacheManager cache;
        private String cacheKeyword;

        private CacheHolder(KeyValueCacheManager cache, String cacheKeyword) {
            this.cache = cache;
            this.cacheKeyword = cacheKeyword;
        }

        public KeyValueCacheManager getCache() {
            return cache;
        }

        public int getSize() {
            return cache.size();
        }

        public String getCacheKeyword() {
            return cacheKeyword;
        }
    }
}
