/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.application.action;

import net.ihe.gazelle.cache.keyvalue.KeyValueCacheManager;
import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.model.ApplicationPreferenceQuery;
import net.ihe.gazelle.common.preference.PreferenceType;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.common.servletfilter.SQLinjectionFilter;
import net.ihe.gazelle.common.util.DateDisplayUtil;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.gazelletest.model.definition.GazelleLanguage;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.mail.ui.context.MailFacesContextImpl;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;

/**
 * <b>Class Description : </b>ApplicationPreferenceManager<br>
 * <br>
 * This class manage the ApplicationPreference object. It corresponds to the Business Layer. All operations to implement are done in this class :
 * <li>Get an ApplicationPreference object</li> <li>
 * etc...</li>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 25
 * @class ApplicationPreferenceManager.java
 * @package net.ihe.gazelle.common.application.action
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@Name("applicationPreferenceManager")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationPreferenceManager")
@MetaInfServices(CSPPoliciesPreferences.class)
public class ApplicationPreferenceManagerImpl implements Serializable, ApplicationPreferenceManager, CSPPoliciesPreferences {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450910163101283760L;

    private static Logger log = LoggerFactory.getLogger(ApplicationPreferenceManager.class);

    public static ApplicationPreferenceManager instance() {

        return (ApplicationPreferenceManager) Component
                .getInstance("applicationPreferenceManager");
    }

    @Override
    public String getStringValue(String key) {
        Object value = load(PreferenceType.STRING, key);
        return (String) value;
    }

    @Override
    public Date getDateValue(String key) {
        Object value = load(PreferenceType.DATETIME, key);
        return (Date) value;
    }

    @Override
    public Integer getIntegerValue(String key) {
        Object value = load(PreferenceType.INTEGER, key);
        return (Integer) value;
    }

    @Override
    public Boolean getBooleanValue(String key) {
        Object value = load(PreferenceType.BOOLEAN, key);
        return value != null && (Boolean) value ;
    }

    private static Object load(final PreferenceType preferenceType, final String key) {

        KeyValueCacheManager longTermCache = ApplicationCacheManager.getApplicationPreferencesCache();
        String value = longTermCache.getValue(key, new net.ihe.gazelle.cache.CacheUpdater() {
            @Override
            public String getValue(String s, Object... o) {
                return realLoadNew(key);
            }
        }, null);
        return preferenceType.asObject(value);
    }

    static String realLoadNew(String key) {
        String value = null;
        ApplicationPreference applicationPreference = get_preference_from_db(key);
        if (applicationPreference != null) {
            value = applicationPreference.getPreferenceValue();
        }
        return value;
    }

    static Object realLoad(PreferenceType preferenceType, String key) {
        Object value = null;
        ApplicationPreference applicationPreference = get_preference_from_db(key);
        if (applicationPreference != null) {
            String valueString = applicationPreference.getPreferenceValue();
            value = preferenceType.asObject(valueString);
        }
        return value;
    }

    public static ApplicationPreference get_preference_from_db(String key) {
        ApplicationPreference applicationPreference = null;
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        Query query = entityManager
                .createQuery("SELECT ap From ApplicationPreference ap WHERE ap.preferenceName = :name");
        query.setParameter("name", key);
        query.setHint("org.hibernate.cacheable", true);

        try {
            applicationPreference = (ApplicationPreference) query.getSingleResult();
        } catch (Exception e) {
            log.error("Failed to load preference with the key : " + key);
        }
        return applicationPreference;
    }

    public static Object typePreference(PreferenceType preferenceType, ApplicationPreference preference) {
        Object value = null;
        if (preference != null) {
            String valueString = preference.getPreferenceValue();
            value = preferenceType.asObject(valueString);
        }
        return value;
    }

    private static String get_application_url_preference() {
        String url = (String) realLoad(PreferenceType.STRING, "application_url");
        if (url == null) {
            try {
                FacesContext fc = FacesContext.getCurrentInstance();
                if (fc != null && !(fc instanceof MailFacesContextImpl)) {

                    ExternalContext ec = fc.getExternalContext();
                    if (ec != null) {
                        url = buildUrlFromRequest(ec);
                    }
                }
            } catch (Exception e) {
                url = null;
            }
        }
        return url;
    }

    private static String buildUrlFromRequest(ExternalContext ec) {
        HttpServletRequest req = (HttpServletRequest) ec.getRequest();
        String scheme = req.getScheme(); // http
        String serverName = req.getServerName(); // hostname.com
        int serverPort = req.getServerPort(); // 80
        String contextPath = req.getContextPath(); // /mywebapp
        StringBuilder url = new StringBuilder();
        url.append(scheme).append("://").append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }

        url.append(contextPath);
        return url.toString();
    }

    public String getApilayerAccessKey() {
        return getStringValue("api_layer_access_key");
    }

    public static String getTimeZoneOffset(TimeZone timeZone) {
        return DateDisplayUtil.getTimeZoneOffset(timeZone);
    }

    public static String getTimeZoneOffset(TimeZone timeZone, Date date) {
        return DateDisplayUtil.getTimeZoneOffset(timeZone, date);
    }

    @Override
    public boolean isPossibleToDeleteOnGMM() {
        return getBooleanValue("delete_on_gmm");
    }

    private PreferenceType getPreferenceTypeByClass(String preferenceClassName) {
        PreferenceType valuePreferenceType = null;
        for (PreferenceType preferenceType : PreferenceType.values()) {
            if (preferenceType.getPreferenceClassName().equals(preferenceClassName)) {
                valuePreferenceType = preferenceType;
            }
        }
        return valuePreferenceType;
    }

    private PreferenceType getPreferenceTypeByValue(Object value) {
        if (value != null) {
            return getPreferenceTypeByClass(value.getClass().getCanonicalName());
        }
        return null;
    }

    @Override
    public Map<String, Object> getValues() {
        HashMap<String, Object> values = new HashMap<String, Object>();

        ApplicationPreferenceQuery query = new ApplicationPreferenceQuery();
        List<ApplicationPreference> preferences = query.getList();

        for (ApplicationPreference preference : preferences) {
            String key = preference.getPreferenceName();
            String value = preference.getPreferenceValue();
            String preferenceClassName = preference.getClassName();

            PreferenceType preferenceType = getPreferenceTypeByClass(preferenceClassName);

            if (preferenceType != null) {
                Object oValue = preferenceType.asObject(value);
                if (!key.equals("application_url_basename")) {
                    values.put(key, oValue);
                }
            }
        }

        return values;
    }

    @Override
    public List<String> persistValues(Map<String, Object> values) {
        List<String> preferences = new ArrayList<String>();
        for (Entry<String, Object> entry : values.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value != null) {
                PreferenceType preferenceType = getPreferenceTypeByValue(value);
                if (preferenceType == null) {
                    log.error("Failed to save preference " + key + ", unknown type : " + value);
                } else {
                    if (key.equals("application_url") && !((String) value).endsWith("/")) {
                        String app_url = (String) value;
                        app_url = app_url + "/";
                        value = app_url;
                    }
                    if (key.equals("tls_url") && ((String) value).endsWith("/")) {
                        String tls_url = (String) value;
                        while (tls_url.endsWith("/")) {
                            tls_url = tls_url.substring(0, tls_url.length() - 1);
                            value = tls_url;
                        }
                    }
                    if (save(preferenceType, key, value)) {
                        preferences.add(key);
                        if (key.equals("application_url")) {
                            preferences = extractBasename(preferences, value, preferenceType);
                        }
                    }
                }
            }
        }
        return preferences;
    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    private List<String> extractBasename(List<String> preferences, Object value, PreferenceType preferenceType) {
        String key2 = "application_url_basename";
        try {
            String test = new URL((String) value).getPath();
            Object value2 = test.substring(1).replace("/", "");
            if (save(preferenceType, key2, value2)) {
                preferences.add(key2);
            }
        } catch (MalformedURLException e) {
            log.error("Unable to extract URL basename from application URL", e);
        }
        return preferences;
    }

    /**
     * Persist an application preference in the underlying database.
     *
     * @param preferenceType Type of the preference.
     * @param key name of the preference.
     * @param value value of the preference (depends on preferenceType).
     * @return true if the preference was already existing and the value has been updated, false otherwise.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private boolean save(PreferenceType preferenceType, String key, Object value) {

        CSPHeaderFilter.clearCache();
        SQLinjectionFilter.resetInjectionFilter();

        boolean updated = false;
        String newValue = preferenceType.asString(value);
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Query query = entityManager
                .createQuery("SELECT ap FROM ApplicationPreference ap where ap.preferenceName = :name");
        query.setParameter("name", key);

        ApplicationPreference applicationPreference = null;
        try {
            applicationPreference = (ApplicationPreference) query.getSingleResult();
        } catch (Exception e) {
            log.warn("Unexpeced error while reading preferences from database:", e);
        }

        if (applicationPreference == null) {
            applicationPreference = new ApplicationPreference();
            applicationPreference.setClassName(preferenceType.getPreferenceClassName());
            applicationPreference.setDescription("");
            applicationPreference.setPreferenceName(key);
            applicationPreference.setPreferenceValue(newValue);
            entityManager.persist(applicationPreference);
        } else {
            String oldValue = applicationPreference.getPreferenceValue();
            if (!StringUtils.equals(oldValue, newValue)) {
                applicationPreference.setPreferenceValue(newValue);
                entityManager.merge(applicationPreference);
                updated = true;
            }
        }
        ApplicationCacheManager.getApplicationPreferencesCache().clearCache();
        return updated;
    }

    @Override
    public void setValue(PreferenceType preferenceType, String key, Object value) {
        if (value != null) {
            save(preferenceType, key, value);
        }
    }

    /**
     * Get the application URL from the database
     *
     * @return applicationUrl
     */
    @Override
    public String getApplicationUrl() {
        String appUrl = get_application_url_preference();
        if (appUrl != null && !appUrl.endsWith("/")) {
            appUrl = appUrl.concat("/");
        }
        return appUrl;
    }

    /**
     * Get the application Name from the database
     *
     * @return applicationName
     */
    @Override
    public String getApplicationName() {
        return getStringValue("application_name");
    }

    /**
     * Get the application administrator name from the database
     *
     * @return applicationAdminNameValue
     */
    @Override
    public String getApplicationAdminName() {
        return getStringValue("application_admin_name");
    }

    @Override
    public String getApplicationAdminTitle() {
        return getStringValue("application_admin_title");
    }

    /**
     * Get the application administrator email from the database
     *
     * @return applicationAdminEmailValue
     */
    @Override
    public String getApplicationAdminEmail() {
        return getStringValue("application_admin_email");
    }

    /**
     * Get the application administrator email from the database
     *
     * @return applicationAdminEmailValue
     */
    @Override
    public String getApplicationNoReplyEmail() {
        return getStringValue("application_no_reply_email");
    }

    /**
     * Get the property name of : email account used for emails history (all emails are sent as BCC to this account)
     *
     * @return applicationEmailAccountForHistoryName : name of this property
     */
    @Override
    public String getApplicationEmailAccountForHistory() {
        return getStringValue("application_email_account_for_history");
    }

    /**
     * Get the application Issue Tracker URL from the database
     *
     * @return applicationIssueTrackerUrlValue : URL of the Issue Tracker (eg : http://sumo.irisa.fr:8080/jira)
     */
    @Override
    public String getApplicationIssueTrackerUrl() {
        return getStringValue("application_issue_tracker_url");
    }

    /**
     * Get the application Gazelle ReleaseNotes URL from the database
     *
     * @return applicationIssueTrackerUrlValue : URL of the Issue Tracker (eg : http://sumo.irisa.fr:8080/jira)
     */
    @Override
    public String getApplicationGazelleReleaseNotesUrl() {
        return getStringValue("application_gazelle_release_notes_url");
    }

    /**
     * Get the application Gazelle Documentation URL from the database
     *
     * @return applicationDocumentationUrlValue : URL of the Documentation
     */
    @Override
    public String getApplicationDocumentationUrl() {
        return getStringValue("application_gazelle_documentation_url");
    }

    /**
     * Get the application Zone from the database
     *
     * @return applicationZone
     */
    @Override
    public String getApplicationZone() {
        return getStringValue("application_zone");
    }

    /**
     * Get the application URL basename from the database (eg. /NA2009)
     *
     * @return applicationUrlBaseNameValue
     */
    @Override
    public String getApplicationUrlBaseName() {
        String[] split = StringUtils.split(getApplicationUrl(), '/');
        if (split.length > 0) {
            return split[split.length - 1];
        } else {
            return "";
        }
    }

    @Override
    public String getApplicationUrlBaseName(EntityManager em) {
        return getApplicationUrlBaseName();
    }

    /**
     * Getters to get Gazelle Paths... JRC 2009, Feb 4th : Current and decided Directory/Files architecture (check your 2009, Feb 4th emails) :
     * Architecture : (for instance, if variable "gazelle.home"
     * = /opt/gazelle_dev) /opt/gazelle_dev/ /opt/gazelle_dev/data /opt/gazelle_dev/data/integrationStatements /opt/gazelle_dev/data/contracts
     * /opt/gazelle_dev/data/invoices
     * /opt/gazelle_dev/data/certificates /opt/gazelle_dev/data/hl7ConformanceStatements /opt/gazelle_dev/data/dicomConformanceStatements
     * /opt/gazelle_dev/reports /opt/gazelle_dev/bin All methods
     * retrieving those values are listed below...
     */

    /**
     * Get the Google Analytic Code from the database
     *
     * @return googleAnalyticsCode
     */
    @Override
    public String getGoogleAnalyticsCode() {
        return getStringValue("google_analytics_code");
    }

    /**
     * Display the Google Analytic Code Tag
     *
     * @return Boolean : true if we display the GoogleAnalytics in the JSF
     */
    @Override
    public Boolean displayGoogleAnalyticsCode() {
        if ((getGoogleAnalyticsCode() == null) || (getGoogleAnalyticsCode().length() < 2)) {
            //
            return false;
        } else {
            //
            return true;
        }
    }

    /**
     * Get the Home path. Returns absolute path for Gazelle Home directory. Example : it returns /opt/gazelle
     *
     * @return String : absolute path for Gazelle home directory : eg /opt/gazelle
     */
    @Override
    public String getGazelleHomePath() {
        return getStringValue("gazelle_home_path");
    }

    /**
     * Get the Data path. Returns absolute path for data directory depending on the Gazelle home Path. Example if Gazelle home is /opt/gazelle, it
     * returns : /opt/gazelle/data
     *
     * @return String : absolute path for data directory : eg /opt/gazelle/data
     */
    @Override
    public String getGazelleDataPath() {
        return getGazelleHomePath() + File.separatorChar + getStringValue("data_path");
    }

    /**
     * Get the Reports path. Returns absolute path for reports directory depending on the Gazelle home Path. Example if Gazelle home is
     * /opt/gazelle, it returns : /opt/gazelle/reports
     *
     * @return String : absolute path for reports directory : eg /opt/gazelle/reports
     */
    @Override
    public String getGazelleReportsPath() {
        return getGazelleHomePath() + File.separatorChar + getStringValue("reports_path");
    }

    /**
     * Get the Bin path. Returns absolute path for bin directory depending on the Gazelle home Path. Example if Gazelle home is /opt/gazelle, it
     * returns : /opt/gazelle/bin
     *
     * @return String : absolute path for bin directory : eg /opt/gazelle/bin
     */
    @Override
    public String getGazelleBinPath() {
        return getGazelleHomePath() + File.separatorChar + getStringValue("bin_path");
    }

    /**
     * Get the IntegrationStatements path. Returns absolute path for IntegrationStatements directory depending on the Gazelle Data Path. Example if
     * Gazelle data is /opt/gazelle/data, it returns :
     * /opt/gazelle/data/integrationStatements
     *
     * @param em EntityManager object
     * @return String : absolute path for IntegrationStatements directory : eg /opt/gazelle/data/integrationStatements
     */
    @Override
    public String getGazelleIntegrationStatementsPath() {
        return getGazelleDataPath() + File.separatorChar + getStringValue("integration_statements_path");
    }

    /**
     * Get the HL7 Conformance Statements path. Returns absolute path for HL7 Conformance Statements directory depending on the Gazelle Data Path.
     * Example if Gazelle data is /opt/gazelle/data, it
     * returns : /opt/gazelle/data/hl7ConformanceStatements
     *
     * @return String : absolute path for HL7 Conformance Statements directory : eg /opt/gazelle/data/hl7ConformanceStatements
     */
    @Override
    public String getGazelleHL7ConformanceStatementsPath() {
        return getGazelleDataPath() + File.separatorChar + getStringValue("hl7_conformance_statements_path");
    }

    /**
     * Get the DICOM Conformance Statements path. Returns absolute path for DICOM Conformance Statements directory depending on the Gazelle Data
     * Path. Example if Gazelle data is /opt/gazelle/data, it
     * returns : /opt/gazelle/data/dicomConformanceStatements
     *
     * @return String : absolute path for DICOM Conformance Statements directory : eg /opt/gazelle/data/dicomConformanceStatements
     */
    @Override
    public String getGazelleDicomConformanceStatementsPath() {
        return getGazelleDataPath() + File.separatorChar + getStringValue("dicom_conformance_statements_path");
    }

    @Override
    public boolean getCASEnabled() {
        return getBooleanValue("cas_enabled");
    }

    @Override
    public String getCASUrl() {
        return getStringValue("cas_url");
    }

    @Override
    public String getEVSClientURL() {
        return getStringValue("evs_client_url");
    }

    @Override
    public String getTlsURL() {
        return getStringValue("tls_url");
    }

    @Override
    public boolean getRulesDB() {
        return getBooleanValue("rules_db");
    }

    @Override
    public boolean getInstallationDone() {
        return getBooleanValue("installation_done");
    }

    @Override
    public boolean isTransactionMonitorEnabled() {
        return getBooleanValue("proxy_transactionmonitor_enabled");
    }

    @Override
    public String getTransactionMonitorUrl() {
        return getStringValue("proxy_transactionmonitor_url");
    }

    @Override
    public boolean isDeployScheduled() {
        return getBooleanValue("deploy_scheduled");
    }

    @Override
    public String getDeploySource() {
        return getStringValue("deploy_source");
    }

    @Override
    public String getDeployTarget() {
        return getStringValue("deploy_target");
    }

    @Override
    public Date getDeployTime() {
        return getDateValue("deploy_time");
    }

    @Override
    public String getRootOid() {
        return getStringValue("root_oid");
    }

    @Override
    public Integer getCATResultCheckInterval() {
        //1000 * 30L;
        return getIntegerValue("cat_result_check_interval");
    }

    @Override
    public Integer getPartnersCheckInterval() {
        return getIntegerValue("partners_check_interval");
    }

    @Override
    public long getServerDateTime() {
        return new Date().getTime();
    }

    @Override
    public long getDeployDateTime() {
        Date dateValue = getDateValue("deploy_time");
        if (dateValue != null) {
            return dateValue.getTime();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isContentPolicyActivated() {
        return getBooleanValue(PreferencesKey.SECURITY_POLICIES.getFriendlyName());
    }

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(),
                getStringValue(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName()));
        headers.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(),
                getStringValue(PreferencesKey.CACHE_CONTROL.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(),
                getStringValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(),
                getStringValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
        headers.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(),
                getStringValue(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(),
                getStringValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(),
                getStringValue(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
        return headers;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return getBooleanValue(PreferencesKey.SQL_INJECTION_FILTER_SWITCH.getFriendlyName());
    }

    @Override
    public void resetHttpHeaders() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        ApplicationPreferenceQuery applicationPreferenceQuery;
        ApplicationPreference ap = null;
        for (PreferencesKey preference : PreferencesKey.values()) {
            applicationPreferenceQuery = new ApplicationPreferenceQuery();
            String friendlyName = preference.getFriendlyName();
            applicationPreferenceQuery.preferenceName().eq(friendlyName);
            ap = applicationPreferenceQuery.getUniqueResult();

            if (ap == null) {
                ap = new ApplicationPreference();
            }
            ap.setClassName(preference.getType());
            ap.setDescription(preference.getDescription());
            ap.setPreferenceName(friendlyName);
            ap.setPreferenceValue(preference.getDefaultValue());
            entityManager.merge(ap);
            entityManager.flush();
        }
        CSPHeaderFilter.clearCache();
    }

    @Override
    public List<SelectItem> getTimeZones() {
        return DateDisplayUtil.getTimeZones();
    }

    @Override
    public List<SelectItem> getLanguages() {
        List<SelectItem> languageItems = new ArrayList<SelectItem>();

        List<GazelleLanguage> languages = GazelleLanguage.getLanguageList();

        languageItems.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.common.PleaseSelect")));
        for (GazelleLanguage language : languages) {
            languageItems.add(new SelectItem(language.getDescription()));
        }
        return languageItems;
    }

    @Override
    public String getDuplicateValues() {
        List<String> duplicates = new ArrayList<String>();

        Set<String> preferenceNames = new TreeSet<String>();
        List<ApplicationPreference> preferences = new ApplicationPreferenceQuery().getList();
        for (ApplicationPreference preference : preferences) {
            String preferenceName = preference.getPreferenceName();
            if (preferenceNames.contains(preferenceName)) {
                duplicates.add(preferenceName);
            }
            preferenceNames.add(preferenceName);
        }

        return StringUtils.join(duplicates, ", ");
    }

    /**
     * This setting can affect server performance as it controls how frequently Proxy channel status is retrieved from the Proxy.
     * Caching will save webservice call trips.  The caching is done at the page-level.  It makes no sense to do it at application-level
     * as the values will become stale very quickly.  When caching is enabled, Ajax-refreshes will get the values from the cache when caching is
     * done at the page-level.  If caching is disabled (the default/recommended setting), then Ajax-refreshes will get the values from Proxy-WS calls.
     *
     * @return boolean true if the status is to be cached; false otherwise
     */
    @Override
    public boolean cacheProxyChannelStatus() {
        return getBooleanValue("cache_proxy_channel_status");
    }

    /**
     * This is the interval in milliseconds that TestInstance.xhtml will auto-refresh the Test results on the screen.
     * <p/>
     * Do not name start JSF methods that load anything with "get" or "is" (for booleans).  The method will end up getting called numerous
     * times via reflection during JSF lifecycle.  You can start the method name with "fetch" or "retrieve" instead.
     *
     * @return the interval in milliseconds
     */
    @Override
    public int retrieveTestResultRefreshInterval() {
        Integer value = getIntegerValue("test_result_refresh_interval");
        if (value == null) {
            value = 2;
        }
        return value * 1000;
    }

    @Override
    public String getCguLink() {
        return getStringValue("link_to_cgu");
    }

}
