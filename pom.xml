<?xml version='1.0' encoding='UTF-8'?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <parent>
        <groupId>net.ihe.gazelle.model</groupId>
        <artifactId>gazelle-model</artifactId>
        <version>8.0.2</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <groupId>net.ihe.gazelle.tm</groupId>
    <artifactId>gazelle-tm-tools</artifactId>
    <version>4.4.3-SNAPSHOT</version>
    <packaging>ejb</packaging>

    <name>gazelle-tm-tools</name>
    <url>http://gazelle.ihe.net</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <git.user.name>gitlab-ci</git.user.name>
        <git.user.token>changeit</git.user.token>
        <git.project.url>
            https://${git.user.name}:${git.user.token}@gitlab.inria.fr/gazelle/public/framework/test-management-tools.git
        </git.project.url>
        <maven.release.plugin.version>2.5.3</maven.release.plugin.version>
        <nexus.staging.maven.plugin.version>1.6.8</nexus.staging.maven.plugin.version>
        <jacoco.version>0.8.1</jacoco.version>
        <skipTests>false</skipTests>
        <hapi.version>2.2</hapi.version>
        <gazelle.plugins.version>1.60</gazelle.plugins.version>
        <powermock.version>1.7.1</powermock.version>
        <mockito.version>1.10.19</mockito.version>
        <sso.client.v7.version>5.0.0</sso.client.v7.version>
    </properties>
    <scm>
        <connection>scm:git:${git.project.url}</connection>
        <url>scm:git:${git.project.url}</url>
        <developerConnection>scm:git:${git.project.url}</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <ciManagement>
        <url>http://gazelle.ihe.net/jenkins/job/gazelle-tm-tools/</url>
        <system>jenkins</system>
    </ciManagement>
    <issueManagement>
        <url>http://gazelle.ihe.net/jira/browse/GZL</url>
        <system>JIRA</system>
    </issueManagement>
    <organization>
        <name>IHE Europe</name>
        <url>http://www.ihe-europe.net</url>
    </organization>


    <build>
        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <systemProperties>
                        <property>
                            <name>java.util.logging.config.file</name>
                            <value>src/test/resources/log4j.properties</value>
                        </property>
                    </systemProperties>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>gazelle-plugins</artifactId>
                <version>${gazelle.plugins.version}</version>
                <configuration>
                    <ignoredParamNames>
                        <ignoredParamName>position</ignoredParamName>
                    </ignoredParamNames>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven.release.plugin.version}</version>
                <configuration>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <releaseProfiles>release</releaseProfiles>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>post-unit-test</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <filtering>true</filtering>
                <directory>src/main/resources</directory>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>false</filtering>
            </testResource>
        </testResources>
    </build>
    <dependencies>
        <!-- junit, powermock -->
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-module-junit4</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-api-mockito</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.hql</groupId>
            <artifactId>gazelle-hql</artifactId>
            <type>jar</type>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.maven</groupId>
            <artifactId>gazelle-tools-jar</artifactId>
            <type>jar</type>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.maven</groupId>
            <artifactId>gazelle-seam-tools-jar</artifactId>
            <type>ejb</type>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.model</groupId>
            <artifactId>gazelle-model-common</artifactId>
            <type>ejb</type>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.model</groupId>
            <artifactId>gazelle-model-tf</artifactId>
            <type>ejb</type>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.model</groupId>
            <artifactId>gazelle-model-users</artifactId>
            <type>ejb</type>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.model</groupId>
            <artifactId>gazelle-model-tm</artifactId>
            <type>ejb</type>
        </dependency>
        <!-- added -->
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>gum-client</artifactId>
            <version>${sso.client.v7.version}</version>
        </dependency>
        <dependency>
            <groupId>org.kohsuke.metainf-services</groupId>
            <artifactId>metainf-services</artifactId>
            <version>1.7</version>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.geoip</groupId>
            <artifactId>gazelle-geoip</artifactId>
            <version>1.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.2.1</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>DICOMEVS-Api</artifactId>
            <version>3.0.13</version>
            <exclusions>
                <exclusion>
                    <artifactId>gazelle-common-jar</artifactId>
                    <groupId>net.ihe.gazelle.modules</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>richfaces-api</artifactId>
                    <groupId>org.richfaces.framework</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>slf4j-log4j12</artifactId>
                    <groupId>org.slf4j</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>ca.uhn.hapi</groupId>
            <artifactId>hapi-base</artifactId>
            <version>${hapi.version}</version>
            <exclusions>
                <exclusion>
                    <artifactId>xml-apis</artifactId>
                    <groupId>xml-apis</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>ca.uhn.hapi</groupId>
            <artifactId>hapi-structures-v231</artifactId>
            <version>${hapi.version}</version>
        </dependency>
        <dependency>
            <groupId>ca.uhn.hapi</groupId>
            <artifactId>hapi-structures-v25</artifactId>
            <version>${hapi.version}</version>
        </dependency>
        <dependency>
            <groupId>gui.ava</groupId>
            <artifactId>html2image</artifactId>
            <version>0.9</version>
        </dependency>
        <dependency>
            <groupId>com.google.zxing</groupId>
            <artifactId>zxing-core</artifactId>
            <version>1.7</version>
        </dependency>
        <dependency>
            <groupId>com.google.zxing</groupId>
            <artifactId>zxing-javase</artifactId>
            <version>1.7</version>
        </dependency>
        <dependency>
            <groupId>bitwalker</groupId>
            <artifactId>UserAgentUtils</artifactId>
            <version>1.8</version>
        </dependency>

        <!-- jasper dependencies -->
        <dependency>
            <groupId>net.sf.jasperreports</groupId>
            <artifactId>jasperreports</artifactId>
            <version>6.4.0</version>
        </dependency>
        <dependency>
            <groupId>net.sf.jasperreports</groupId>
            <artifactId>jasperreports-functions</artifactId>
            <version>6.4.0</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
            <version>2.4.5</version>
        </dependency>
        <!-- end of jasper dependencies -->
        <dependency>
            <groupId>eclipse</groupId>
            <artifactId>jdtcore</artifactId>
            <version>3.1.0</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.jasper</groupId>
            <artifactId>gazelle-jasper-fonts</artifactId>
            <version>1.0</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.modules</groupId>
            <artifactId>gazelle-ws-clients</artifactId>
            <version>3.2.3</version>
        </dependency>
        <dependency>
            <groupId>uk.com.robust-it</groupId>
            <artifactId>cloning</artifactId>
            <version>1.9.0</version>
        </dependency>
        <!-- Seam dependencies (Seam, Richfaces, Hibernate, Servlet, JSF, JEE,
                ...) -->
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam-remoting</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam-ui</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam-mail</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam-debug</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam-pdf</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.seam</groupId>
            <artifactId>jboss-seam-excel</artifactId>
        </dependency>
        <dependency>
            <groupId>org.codelibs</groupId>
            <artifactId>jhighlight</artifactId>
            <version>1.0.3</version>
        </dependency>
        <dependency>
            <groupId>org.jboss.el</groupId>
            <artifactId>jboss-el</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-beanutils</groupId>
            <artifactId>commons-beanutils</artifactId>
        </dependency>
        <!-- Provided -->
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.jboss.spec.javax.el</groupId>
            <artifactId>jboss-el-api_2.2_spec</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-entitymanager</artifactId>
            <version>5.4.4.Final</version>
            <exclusions>
                <exclusion>
                    <artifactId>xml-apis</artifactId>
                    <groupId>xml-apis</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <version>2.0.1.Final</version>
        </dependency>

        <dependency>
            <groupId>sun-jaxb</groupId>
            <artifactId>jaxb-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.mail</groupId>
            <artifactId>mail</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
        </dependency>
        <!-- Test dependencies -->
        <dependency>
            <groupId>net.ihe.gazelle.model</groupId>
            <artifactId>gazelle-model-common</artifactId>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.sf.ehcache</groupId>
            <artifactId>ehcache</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>c3p0</groupId>
            <artifactId>c3p0</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-c3p0</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.richfaces.ui</groupId>
            <artifactId>richfaces-components-ui</artifactId>
        </dependency>
        <dependency>
            <groupId>org.richfaces.core</groupId>
            <artifactId>richfaces-core-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sun.faces</groupId>
            <artifactId>jsf-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jboss.spec.javax.ejb</groupId>
            <artifactId>jboss-ejb-api_3.1_spec</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>5.4.4.Final</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate.common</groupId>
            <artifactId>hibernate-commons-annotations</artifactId>
            <version>5.0.1.Final</version>
        </dependency>
        <dependency>
            <groupId>org.jboss.logging</groupId>
            <artifactId>jboss-logging</artifactId>
            <version>3.2.0.Final</version>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.4.200</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.cache</groupId>
            <artifactId>gazelle-cache</artifactId>
            <version>1.0.4</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle.junit</groupId>
            <artifactId>gazelle-junit</artifactId>
            <version>1.0.20</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>gazelle-plugins</artifactId>
            <version>${gazelle.plugins.version}</version>
        </dependency>
        <dependency>
            <groupId>xalan</groupId>
            <artifactId>xalan</artifactId>
            <version>2.7.0</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>IHE</id>
            <name>IHE Public Maven Repository Group</name>
            <url>https://nexus.ihe-catalyst.net/repository/maven-public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>IHE-plugins</id>
            <name>IHE Plugins Public Maven Repository Group</name>
            <url>https://nexus.ihe-catalyst.net/repository/maven-public/</url>
            <layout>default</layout>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <distributionManagement>
        <repository>
            <id>nexus-releases</id>
            <url>https://nexus.ihe-catalyst.net/repository/releases/</url>
        </repository>
        <snapshotRepository>
            <id>nexus-snapshots</id>
            <url>https://nexus.ihe-catalyst.net/repository/snapshots/</url>
        </snapshotRepository>
    </distributionManagement>

    <profiles>
        <profile>
            <id>dev</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <profile>
            <id>sonar</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonarsource.scanner.maven</groupId>
                        <artifactId>sonar-maven-plugin</artifactId>
                        <version>${sonar.maven.plugin}</version>
                        <executions>
                            <execution>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sonar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>${nexus.staging.maven.plugin.version}</version>
                        <executions>
                            <execution>
                                <id>default-deploy</id>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <serverId>nexus-releases</serverId>
                            <nexusUrl>https://gazelle.ihe.net/nexus</nexusUrl>
                            <skipStaging>true</skipStaging>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
